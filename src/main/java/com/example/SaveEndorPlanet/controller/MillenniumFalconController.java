package com.example.SaveEndorPlanet.controller;

import com.example.SaveEndorPlanet.service.MillenniumFalconService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class MillenniumFalconController {

    @Autowired
    private MillenniumFalconService millenniumFalconService;


    //  Call this REST endpoints to start the mission and to get the results.
    @GetMapping("/start-mission")
    public Double startMission() throws IOException {
        double successProbability;

        // Call the service to calculate the success probability of the mission


        // Return the result to the user
        return  millenniumFalconService.calculateSuccessProbability();
    }
    @GetMapping("")
    public String welcomeToMillenniumFalconMission() {

        //  Welcoming
        return "Welcome To Millennium Falcon Mission. Please call http://localhost:8080/start-mission to start the mission";
    }
}
