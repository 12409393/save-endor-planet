package com.example.SaveEndorPlanet.service;

import com.example.SaveEndorPlanet.model.Empire;
import com.example.SaveEndorPlanet.model.MillenniumFalcon;
import com.example.SaveEndorPlanet.util.JsonFileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class MillenniumFalconService {
    private static final Logger log = LoggerFactory.getLogger(MillenniumFalconService.class);

    @Autowired
    private JsonFileReader jsonFileReader;  // Dependency injection of JSON file reader

    private MillenniumFalcon millenniumFalcon;  // Holds Millennium Falcon data
    private Empire empire;  // Holds Empire data

    // Loads mission data from JSON files
    public void loadMissionData() throws IOException {
        millenniumFalcon = jsonFileReader.readJsonFile("millennium-falcon.json", MillenniumFalcon.class);
        empire = jsonFileReader.readJsonFile("empire.json", Empire.class);
    }

    // Finds all possible routes from Tatooine to Endor
    public List<List<MillenniumFalcon.Route>> findAllRoutes(List<MillenniumFalcon.Route> routes) {
        List<List<MillenniumFalcon.Route>> allPaths = new ArrayList<>();  // To store all found routes
        List<MillenniumFalcon.Route> currentPath = new ArrayList<>();  // To store the current route being explored

        // Start Depth-First Search (DFS) from Tatooine to Endor
        dfs("Tatooine", "Endor", routes, currentPath, allPaths);

        return allPaths;  // Return the list of all found routes
    }

    // Private method for depth-first search to explore routes
    private void dfs(String currentPlanet, String destinationPlanet,
                     List<MillenniumFalcon.Route> routes,
                     List<MillenniumFalcon.Route> currentPath,
                     List<List<MillenniumFalcon.Route>> allPaths) {
        // If we have reached the destination, add the current path to the result
        if (currentPlanet.equals(destinationPlanet)) {
            allPaths.add(new ArrayList<>(currentPath));  // Add a copy of the current path
            return;  // Exit the function
        }

        // Explore each route
        for (MillenniumFalcon.Route route : routes) {
            // If this route starts at the current planet, explore it
            if (route.getOrigin().equals(currentPlanet)) {
                currentPath.add(route);  // Add the route to the current path

                // Recursively explore the next planet using the destination of the current route
                dfs(route.getDestination(), destinationPlanet, routes, currentPath, allPaths);

                // Backtrack by removing the last added route (backtracking step)
                currentPath.remove(currentPath.size() - 1);
            }
        }
    }

    // Method to calculate the success probability of reaching Endor
    public double calculateSuccessProbability() {
        try {
            loadMissionData();  // Load mission data from JSON files
        } catch (IOException e) {
            throw new RuntimeException(e);  // Handle exceptions when loading data
        }

        // Find all possible routes from Tatooine to Endor
        List<List<MillenniumFalcon.Route>> allPaths = findAllRoutes(millenniumFalcon.getRoutes());

        // Initialize tracking structures for stops and travel times
        List<List<Empire.BountyHunter>> millenniumFalconStops = new ArrayList<>();
        for (int i = 0; i < allPaths.size(); i++) {
            millenniumFalconStops.add(new ArrayList<>());  // Initialize an empty stop list for each route
        }

        int autonomy = millenniumFalcon.getAutonomy();  // Get the autonomy of the Millennium Falcon
        List<Integer> countTravelTimeForAllPossibleRoute = new ArrayList<>(Collections.nCopies(allPaths.size(), 0));  // Track travel times for each route

        // Iterate through each route in allPaths
        for (int i = 0; i < allPaths.size(); i++) {
            List<MillenniumFalcon.Route> currentPath = allPaths.get(i);  // Get the current path
            int currentAutonomy = autonomy;  // Reset autonomy for each path

            // Iterate through the routes in the current path
            for (int j = 0; j < currentPath.size(); j++) {
                MillenniumFalcon.Route currentRoute = currentPath.get(j);  // Get the current route

                // Update travel time for the current route
                countTravelTimeForAllPossibleRoute.set(i, countTravelTimeForAllPossibleRoute.get(i) + currentRoute.getTravelTime());

                currentAutonomy -= currentRoute.getTravelTime();  // Decrease autonomy based on the travel time

                // Add stop at the current planet with the current travel time
                millenniumFalconStops.get(i).add(new Empire.BountyHunter(currentRoute.getDestination(), countTravelTimeForAllPossibleRoute.get(i)));

                // Check if autonomy is exhausted and refuel if necessary
                if (currentAutonomy <= 0) {
                    // Refuel: Add 1 day for refueling, record the stop
                    countTravelTimeForAllPossibleRoute.set(i, countTravelTimeForAllPossibleRoute.get(i) + 1);  // Refueling takes a full day
                    millenniumFalconStops.get(i).add(new Empire.BountyHunter(currentRoute.getDestination(), countTravelTimeForAllPossibleRoute.get(i)));

                    // Reset autonomy after refueling
                    currentAutonomy = millenniumFalcon.getAutonomy();
                }
            }
        }

        // Investigate all the possible routes to analyze which one is the best to save Endor
        List<Empire.BountyHunter> bountyHunters = empire.getBountyHunters();  // Get the list of bounty hunters
        List<Integer> captureTimeForAllPaths = new ArrayList<>(Collections.nCopies(allPaths.size(), 0));  // Track capture times for each path

        // Check if any bounty hunter captures the Falcon at the various stops
        for (int k = 0; k < millenniumFalconStops.size(); k++) {
            for (Empire.BountyHunter millenniumFalconStop : millenniumFalconStops.get(k)) {
                if (bountyHunters.contains(millenniumFalconStop)) {
                    // Increase capture time if a bounty hunter is at the same stop
                    captureTimeForAllPaths.set(k, captureTimeForAllPaths.get(k) + 1);
                }
            }
        }

        // Countdown from the Empire
        int countDown = empire.getCountdown();
        // Initialize probabilities of all possible paths
        List<Double> allPossibleProbabilities = new ArrayList<>(Collections.nCopies(allPaths.size(), 0.00));

        // Calculate the success probabilities for each route based on travel time and capture time
        for (int i = 0; i < countTravelTimeForAllPossibleRoute.size(); i++) {
            // Check if the travel time is within the countdown
            if (countTravelTimeForAllPossibleRoute.get(i) <= countDown) {
                allPossibleProbabilities.set(i, 1 - chanceOfCapture(captureTimeForAllPaths.get(i)));  // Calculate success probability
            }
        }

        //check which path will be Perfect to wait on. If Not don't wait.
//        for (int i = 0; i < countTravelTimeForAllPossibleRoute.size() ; i++) {
//            if(countTravelTimeForAllPossibleRoute.get(i) - countDown > 0 ){
//                countTravelTimeForAllPossibleRoute
//            }
//
//        }


        // Print and return the maximum success probability
        return allPossibleProbabilities.stream().max(Double::compare).orElse(0.0);
    }

    // Method to calculate the chance of capture based on the number of times captured
    public double chanceOfCapture(int capturedTimes) {
        // Base case: when k == 0, the probability of being captured is 0
        if (capturedTimes == 0) {
            return 0;
        }
        // Base case: when k == 1, the probability of being captured is 1/10
        if (capturedTimes == 1) {
            return 1.0 / 10;
        }
        // Recursive case: compute the current term and add it to the previous ones
        else {
            return Math.pow(9, capturedTimes - 1) / Math.pow(10, capturedTimes) + chanceOfCapture(capturedTimes - 1);
        }
    }
}
