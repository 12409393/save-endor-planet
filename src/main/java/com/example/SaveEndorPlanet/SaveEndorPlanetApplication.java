package com.example.SaveEndorPlanet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SaveEndorPlanetApplication {

	public static void main(String[] args) {
		SpringApplication.run(SaveEndorPlanetApplication.class, args);
	}

}
