package com.example.SaveEndorPlanet.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class JsonFileReader {

    private final ObjectMapper objectMapper = new ObjectMapper();

    public <T> T readJsonFile(String fileName, Class<T> clazz) throws IOException {
        // Load the file from classpath (resources folder)
        ClassPathResource resource = new ClassPathResource("data/" + fileName);

        // Read and parse the JSON file
        return objectMapper.readValue(resource.getInputStream(), clazz);
    }
}
