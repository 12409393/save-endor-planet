package com.example.SaveEndorPlanet.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data // Generates Getters, Setters, ToString, EqualsAndHashCode....
@NoArgsConstructor // Generates a no-args constructor
@AllArgsConstructor // Generates an all-args constructor
public class MillenniumFalcon {
    @JsonProperty("autonomy")
    private int autonomy;
    @JsonProperty("routes")
    private List<Route> routes;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Route {
        @JsonProperty("origin")
        private String origin;
        @JsonProperty("destination")
        private String destination;
        @JsonProperty("travelTime")
        private int travelTime;
    }
}
