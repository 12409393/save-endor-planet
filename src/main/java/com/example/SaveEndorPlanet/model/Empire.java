package com.example.SaveEndorPlanet.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Empire {
    @JsonProperty("countdown")
    private int countdown;
    @JsonProperty("bounty_hunters")
    private List<BountyHunter> bountyHunters;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class BountyHunter {
        @JsonProperty("planet")
        private String planet;
        @JsonProperty("day")
        private int day;
    }
}
