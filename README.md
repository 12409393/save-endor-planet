# Save Endor Planet

## Overview

The **Save Endor Planet** application is a simulation game where players control the Millennium Falcon on a mission to reach the planet Endor while avoiding capture by the Empire's bounty hunters. The objective is to calculate the probability of successfully reaching Endor before the Death Star.

## Features

- **Route Calculation**: Find all possible routes from Tatooine to Endor.
- **Success Probability**: Calculate the probability of reaching Endor based on travel time and the number of captures by bounty hunters.
- **Autonomy Management**: Manage the Millennium Falcon's autonomy and refuel as needed during the journey.
- **Dynamic Data**: Load mission data from JSON files to customize the simulation.

## Prerequisites

- Java Development Kit (JDK) 17 or later
- Gradle (for dependency management and building)
- IDE (IntelliJ IDEA)
### Clone the Repository

```bash
git clone https://github.com/yourusername/save-endor-planet.git
cd save-endor-planet
```
# Customizing JSON Files for Simulation

To effectively simulate different scenarios and outputs in the **Save Endor Planet** application, you may need to modify the JSON files located in the `src/main/resources/data` directory. The two primary JSON files you will need to update `millennium-falcon.json` and `empire.json`.

## Example JSON Structure



This file contains the properties of the Millennium Falcon, including the routes and its autonomy. Below is an example of the JSON structure:
### empire.json
```json
{
  "countdown": 9,
  "bounty_hunters": [
    {"planet": "Hoth", "day": 6 },
    {"planet": "Hoth", "day": 7 },
    {"planet": "Hoth", "day": 8 }
  ]
}

```
### millennium-falcon.json
```json
{
  "routes": [
    {"origin": "Tatooine", "destination": "Dagobah", "travelTime": 6},
    {"origin": "Dagobah", "destination": "Endor", "travelTime": 4},
    {"origin": "Tatooine", "destination": "Hoth", "travelTime": 6},
    {"origin": "Hoth", "destination": "Endor", "travelTime": 1}
  ],
  "autonomy": 10
} 
```
# MIT License

Copyright (c) 2024 AMAL Hajar





